let softdrinksList = [
    {
        "name": "Coke",
        "prize": "2$",
        "id": 1,
        "imageUrl": "https://farm1.staticflickr.com/71/203324363_b448827eb0.jpg",
        "volume": "50cl"
    },
    {
        "name": "Fanta",
        "prize": "2$",
        "id": 2,
        "imageUrl": "https://farm1.staticflickr.com/684/32876893826_130576f75a.jpg",
        "volume": "50cl"
    },
    {
        "name": "Pepsi",
        "prize": "2$",
        "id": 3,
        "imageUrl": "https://farm4.staticflickr.com/3344/3593103557_bf47c0a3a2.jpg",
        "volume": "50cl"
    },
    {
        "name": "Red bull",
        "prize": "3$",
        "id": 4,
        "imageUrl": "https://farm3.staticflickr.com/2391/2507916617_254348d40c.jpg",
        "volume": "50cl"
    }
];


const softdrinksContainer = document.getElementById("softdrinksContainer");


softdrinksList.forEach(function (softdrink) {
    var softdrinkDiv = document.createElement("div");
    softdrinkDiv.className = "pizzaDiv";
    softdrinkDiv.innerHTML = '<img src="' + softdrink.imageUrl + '" class="picturesForPagesWithProducts"><h2>' + softdrink.name +
        '</h2><div class="textunderpictures"><select id="dressing_fall" name = "dressing_fall"><option value="' + softdrink.volume + '">' + softdrink.volume + '</option><option value="100cl">100cl</option></select><h2 class="price">' + softdrink.prize + '</h2><button onclick="addToCart(' + softdrink.id + ')"><img src="../Bilder/shoppingcart.png" class="schoppingcartImg"></button></div></div>';
        softdrinksContainer.appendChild(softdrinkDiv);

});


const selectedInShoppingCartDiv = document.getElementById("selectedInShoppingCart");


function addToCart(softdrinkId) {
    var NumberOfElementsInShoppingCart = parseInt(document.getElementById("NumberOfElementsInShoppingCart").innerHTML);
    softdrinksList.forEach(softdrink => {
        if (softdrink.id == softdrinkId) {
            NumberOfElementsInShoppingCart += 1;
            document.getElementById("NumberOfElementsInShoppingCart").innerHTML = NumberOfElementsInShoppingCart;
            var softdrinksListItem = document.createElement("div");
            softdrinksListItem.textContent = softdrink.name + " - " + softdrink.prize;
            selectedInShoppingCartDiv.appendChild(softdrinksListItem);
            var carTotalValue = parseInt(document.getElementById("cartTotal").innerHTML) + parseInt(softdrink.prize);
            document.getElementById("cartTotal").innerHTML = carTotalValue;
        }
    });
}


function OrderBtn() {
    if (parseInt(document.getElementById("cartTotal").innerHTML) == 0) { // zu korregieren
        alert("Please select a drink to order");
    } else {
        toggleCart();
        ResetCart();
        alert("You have succsessfully ordered. Thank you!");
    }
}


function toggleCart() {
    const cartContent = document.getElementById("cartContent");
    cartContent.style.display = cartContent.style.display === "none" ? "flex" : "none";
}


function ResetCart() {
    selectedInShoppingCartDiv.innerHTML = "";
    document.getElementById("NumberOfElementsInShoppingCart").innerHTML = "0"; 
    document.getElementById("cartTotal").innerHTML = "0";
}

