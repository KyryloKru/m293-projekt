let saladList = [
    {
        "name": "Green salad with tomatoe",
        "prize": "4$",
        "id": 1,
        "ingredients": [
            "Iceberg lettuce",
            "Tomatoes"
        ],
        "imageUrl": "https://farm6.staticflickr.com/5087/5358599242_7251dc7de4.jpg"
    },
    {
        "name": "Tomato salad with mozzarella",
        "prize": "5$",
        "id": 2,
        "ingredients": [
            "Tomato",
            "Mozzarella"
        ],
        "imageUrl": "https://farm4.staticflickr.com/3130/5862973974_c107ed81ea.jpg"
    },
    {
        "name": "Field salad with egg",
        "prize": "4$",
        "id": 3,
        "ingredients": [
            "Field salad",
            "Egg"
        ],
        "imageUrl": "https://farm9.staticflickr.com/8223/8372222471_662acd24f6.jpg"
    },
    {
        "name": "Rocket with parmesan",
        "prize": "5$",
        "id": 4,
        "ingredients": [
            "Rocket",
            "Parmesan"
        ],
        "imageUrl": "https://farm8.staticflickr.com/7017/6818343859_bb69394ff2.jpg"
    }
];


const saladContainer = document.getElementById("saladContainer");


saladList.forEach(function (salad) {
    var saladDiv = document.createElement("div");
    saladDiv.className = "pizzaDiv";
    saladDiv.innerHTML = '<img src="' + salad.imageUrl + '" class="picturesForPagesWithProducts"><h2>' + salad.name +
        '</h2><div class="textunderpictures"><select id="dressing_fall" name = "dressing_fall"><option value="Italian Dressing">Italian Dressing</option><option value="French Dressing">French Dressing</option><option value="No Dressing">No Dressing</option></select><h2 class="price">' + salad.prize + '</h2><button onclick="addToCart(' + salad.id + ')"><img src="../Bilder/shoppingcart.png" class="schoppingcartImg"></button></div></div><div class="description">' + salad.ingredients + '</div>'
    saladContainer.appendChild(saladDiv);
});


const selectedInShoppingCartDiv = document.getElementById("selectedInShoppingCart");

function addToCart(saladId) {
    var NumberOfElementsInShoppingCart = parseInt(document.getElementById("NumberOfElementsInShoppingCart").innerHTML);
    saladList.forEach(salad => {
        if (salad.id == saladId) {
            NumberOfElementsInShoppingCart += 1;
            document.getElementById("NumberOfElementsInShoppingCart").innerHTML = NumberOfElementsInShoppingCart;
            var saladListItem = document.createElement("div");
            saladListItem.textContent = salad.name + " - " + salad.prize;
            selectedInShoppingCartDiv.appendChild(saladListItem);
            var carTotalValue = parseInt(document.getElementById("cartTotal").innerHTML) + parseInt(salad.prize);
            document.getElementById("cartTotal").innerHTML = carTotalValue;
        }
    });
}


function OrderBtn() {
    if (parseInt(document.getElementById("cartTotal").innerHTML) == 0) { // zu korregieren
        alert("Please select a salad to order");
    } else {
        toggleCart();
        ResetCart();
        alert("You have succsessfully ordered. Thank you!");
    }
}


function toggleCart() {
    const cartContent = document.getElementById("cartContent");
    cartContent.style.display = cartContent.style.display === "none" ? "flex" : "none";
}


function ResetCart() {
    selectedInShoppingCartDiv.innerHTML = "";
    document.getElementById("NumberOfElementsInShoppingCart").innerHTML = "0";
    document.getElementById("cartTotal").innerHTML = "0";
}

