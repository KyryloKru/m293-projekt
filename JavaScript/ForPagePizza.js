let pizzaList = [
    {
        "name": "Piccante",
        "prize": "16$",
        "id": 1,
        "ingredients": [
            "Tomato",
            "Mozzarella",
            "Spicy Salami",
            "Chilies",
            "Oregano"
        ],
        "imageUrl": "https://farm5.staticflickr.com/4042/4660357797_09dcd917b1.jpg"
    },
    {
        "name": "Giardino",
        "prize": "14$",
        "id": 2,
        "ingredients": [
            "Tomato",
            "Mozzarella",
            "Artichokes",
            "Fresh Mushrooms"
        ],
        "imageUrl": "https://farm4.staticflickr.com/3565/5818252079_29635c03cc.jpg"
    },
    {
        "name": "Prosciuotto e funghi",
        "prize": "15$",
        "id": 3,
        "ingredients": [
            "Tomato",
            "Mozzarella",
            "Ham",
            "Fresh Mushrooms",
            "Oregano"
        ],
        "imageUrl": "https://farm9.staticflickr.com/8326/8096659940_4e0a65e598.jpg"
    },
    {
        "name": "Quattro formaggi",
        "prize": "13$",
        "id": 4,
        "ingredients": [
            "Tomato",
            "Mozzarella",
            "Parmesan",
            "Gorgonzola"
        ],
        "imageUrl": "https://farm3.staticflickr.com/2797/4344770705_b6d159f799.jpg"
    },
    {
        "name": "Quattro stagioni",
        "prize": "17$",
        "id": 5,
        "ingredients": [
            "Tomato",
            "Mozzarella",
            "Ham",
            "Artichokes",
            "Fresh Mushrooms"
        ],
        "imageUrl": "https://farm5.staticflickr.com/4078/4932649252_b0aaa733ae.jpg"
    },
    {
        "name": "Stromboli",
        "prize": "12$",
        "id": 6,
        "ingredients": [
            "Tomato",
            "Mozzarella",
            "Fresh Chilies",
            "Olives",
            "Oregano"
        ],
        "imageUrl": "https://farm6.staticflickr.com/5661/22907779119_b2ec1efa11.jpg"
    },
    {
        "name": "Verde",
        "prize": "13$",
        "id": 7,
        "ingredients": [
            "Tomato",
            "Mozzarella",
            "Broccoli",
            "Spinach",
            "Oregano"
        ],
        "imageUrl": "https://farm7.staticflickr.com/6044/6363618775_e8714fb517.jpg"
    },
    {
        "name": "Rustica",
        "prize": "15$",
        "id": 8,
        "ingredients": [
            "Tomato",
            "Mozzarella",
            "Ham",
            "Bacon",
            "Onions",
            "Garlic",
            "Oregano"
        ],
        "imageUrl": "https://www.shelovesbiscotti.com/wp-content/uploads/2017/04/IMG_5119.jpg"
    }
];


const pizzaContainer = document.getElementById("pizzaContainer");


pizzaList.forEach(function (pizza) {
    var pizzaDiv = document.createElement("div");
    pizzaDiv.className = "pizzaDiv";
    pizzaDiv.innerHTML = '<img src="' + pizza.imageUrl + '" class="picturesForPagesWithProducts"><div class="textunderpictures"><h2>' + pizza.name +
        '</h2><h2 class="price">' + pizza.prize + '</h2><button onclick="addToCart(' + pizza.id + ')"><img src="../Bilder/shoppingcart.png" class="schoppingcartImg"></button></div><div class="description">' + pizza.ingredients + '</div>'
    pizzaContainer.appendChild(pizzaDiv);
});


const selectedInShoppingCartDiv = document.getElementById("selectedInShoppingCart");


function addToCart(pizzaId) {
    var NumberOfElementsInShoppingCart = parseInt(document.getElementById("NumberOfElementsInShoppingCart").innerHTML);
    pizzaList.forEach(pizza => {
        if (pizza.id == pizzaId) {
            NumberOfElementsInShoppingCart += 1;
            document.getElementById("NumberOfElementsInShoppingCart").innerHTML = NumberOfElementsInShoppingCart;
            var pizzaListItem = document.createElement("div");
            pizzaListItem.textContent = pizza.name + " - " + pizza.prize;
            selectedInShoppingCartDiv.appendChild(pizzaListItem);
            var carTotalValue = parseInt(document.getElementById("cartTotal").innerHTML) + parseInt(pizza.prize);
            document.getElementById("cartTotal").innerHTML = carTotalValue;
        }
    });
}


function OrderBtn() {
    if (parseInt(document.getElementById("cartTotal").innerHTML) == 0) { 
        alert("Please select a pizza to order");
    } else {
        toggleCart();
        ResetCart();
        alert("You have succsessfully ordered. Thank you!");
    }
}


function toggleCart() {
    const cartContent = document.getElementById("cartContent");
    cartContent.style.display = cartContent.style.display === "none" ? "flex" : "none";
}


function ResetCart() {
    selectedInShoppingCartDiv.innerHTML = "";
    document.getElementById("NumberOfElementsInShoppingCart").innerHTML = "0";
    document.getElementById("cartTotal").innerHTML = "0";
}
